import {UserController} from './controllers/user';
import {OperationsController} from './controllers/operations';
import {OperationController} from './controllers/operation';
import {BrawlController} from './controllers/brawl';

export const setSubscribers = io => connection => {
  io.on('connection', socket => {  
    const user = new UserController(socket, connection);
    const brawl = new BrawlController(socket, connection);
    const operation = new OperationController(socket, connection);
    const operations = new OperationsController(socket, connection);

    socket.on('connect-user', user.connect);

    socket.on('subscribe-for-operations', operations.subscribe);

    socket.on('set-operation', operation.set);
    socket.on('join-operation', operation.join);
    socket.on('accept-operation', operation.accept);
    socket.on('lock-operation', operation.lock);
    socket.on('subscribe-for-operation', operation.subscribe);

    socket.on('brawl-set', brawl.set);
    socket.on('brawl-set-ship', brawl.setShip);
    socket.on('brawl-shoot', brawl.shoot);
    socket.on('brawl-damage', brawl.damage);
    socket.on('subscribe-for-brawl', brawl.subscribe);
  });
};