export const SHIPS_MAX_SIZE = 10;

export const SHIPS_LIMITS = {
  '1': 4,
  '2': 3,
  '3': 2,
  '4': 1,
};

export const haveAllShipsBeenDestroyed = ships => ships.every(ship => ship.destroyed);

