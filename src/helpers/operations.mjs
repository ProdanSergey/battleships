export const isOperationLocked = (players = {}) => 
  Object.values(players).every(player => player.locked);

export const randomizeInitialTurn = (players = {}) => {
  const keys = Object.keys(players);

  return keys[Math.floor(Math.random() * keys.length)];
};