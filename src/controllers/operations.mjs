import r from 'rethinkdb';

export class OperationsController {
  constructor(socket, connection) {
    this.socket = socket;
    this.connection = connection;
    this.table = 'operations';
  }

  subscribe = () => {
    r.table(this.table)
      .filter({stage: 'new'})
      // .orderBy(r.desc('timestamp'))
      .changes({include_initial: true, include_types: true, include_states: true, squash: true})
      .run(this.connection)
      .then(cursor => {
        const initial_val = [];

        cursor.each((err, {old_val, new_val, state, type}) => {
          switch (type) {
            case 'initial':
              initial_val.push(new_val);
              break;
            case 'state':
              if (state === 'ready') {
                this.socket.emit('OPERATIONS', initial_val);
              }
              break;
            case 'add':
              this.socket.emit('OPERATION_STARTED', new_val);
              break;
            case 'remove':
              this.socket.emit('OPERATION_CANCELED', old_val.id);
              break;
          }
        })
      })
  }
};