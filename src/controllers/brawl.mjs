import r from 'rethinkdb';
import {SHIPS_MAX_SIZE, haveAllShipsBeenDestroyed} from '../helpers/brawl';

export class BrawlController {
  constructor(socket, connection) {
    this.socket = socket;
    this.connection = connection;
  }

  set = ({operationId, userId}) => {
    r.table('brawls')
      .insert({
        operationId,
        userId,
        ready: false,
        ships: [],
        shoots: [],
        damage: [],
        stage: 'new',
      })
      .run(this.connection)
      .then(({generated_keys}) => {
        this.socket.emit('BRAWL_STARTED', generated_keys[0]);
      });
  }

  setShip = ({brawlId, ship}) => {
    r.table('brawls')
      .get(brawlId)
      .update(brawl => r.branch(
        brawl('ships').count().lt(SHIPS_MAX_SIZE),
        brawl.merge({
          ships: brawl('ships').append(ship)
        }).merge(new_value => r.branch(
          new_value('ships').count().eq(SHIPS_MAX_SIZE),
          { stage: 'ready', ready: true },
          new_value,
        )),
        r.error('Can\'t add more ships')
      ))
      .run(this.connection);
  }

  shoot = ({brawlId, shoot}) => {
    r.table('brawls')
      .get(brawlId)
      .update(
        {shoots: r.row('shoots').append(shoot.key)}, 
        {return_changes: true},
      )
      .run(this.connection)
      .then(({changes}) => {
        changes.forEach(({new_val}) => {
          this.socket.to(new_val.operationId).emit('BRAWL_SHOT_COMMITED', shoot);
        });
      });
  }

  damage = ({brawlId, shoot}) => {
    r.table('brawls')
      .get(brawlId)
      .update(
        brawl => ({
          damage: brawl('damage').append(shoot.key),
          ships: brawl('ships').map(ship => r.branch(
            ship('gps').contains(shoot.key),
            ship.merge({decks: {[shoot.key]: true}}).merge(
              new_value => ({
                destroyed: new_value('decks').values().contains(deck => deck.not()).not()
              })
            ),
            ship
          ))
        }),
        {return_changes: true},
      )
      .run(this.connection)
      .then(({changes}) => {
        changes.forEach(({new_val}) => {
          const damagedShip = new_val.ships.find(ship => ship.gps.includes(shoot.key))

          if (damagedShip) {
            this.socket.to(new_val.operationId).emit(
              'BRAWL_SHIP_DAMAGE_COMMITED', 
              shoot.key, 
              damagedShip.destroyed && damagedShip.gps
            );
          } else {
            this.socket.emit('BRAWL_TURN');
            this.socket.to(new_val.operationId).emit('BRAWL_MISS_COMMITED');
          }
        });
      });
  }

  subscribe = async brawlId => {
    const cursor = await r.table('brawls')
      .get(brawlId)
      .changes({include_initial: true})
      .run(this.connection);
    
      cursor.each((err, {old_val, new_val}) => {
      this.socket.emit('BRAWL_COMMITED', new_val);

      if (old_val && !old_val.ready && new_val.ready) {
        this.socket.server.in(new_val.operationId).emit('BRAWL_PLAYER_READY', new_val.userId);
      }

      if (new_val.ready && haveAllShipsBeenDestroyed(new_val.ships)) {
        this.socket.server.in(new_val.operationId).emit('BRAWL_PLAYER_DEFEATED', new_val.userId);

        r.table('operations')
          .get(new_val.operationId)
          .update({
            stage: 'completed',
            players: {
              [new_val.userId]: {
                defeated: true,
              },
            }
          })
          .run(this.connection);
      }
    })      
  }
};