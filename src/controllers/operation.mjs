import r from 'rethinkdb';
import {isOperationLocked, randomizeInitialTurn} from '../helpers/operations';

export class OperationController {
  constructor(socket, connection) {
    this.socket = socket;
    this.connection = connection;
    this.table = 'operations';
  }

  set = ({operation, userId}) => {
    r.table(this.table)
      .insert({
        ...operation,
        host: r.table('users').get(userId),
        timestamp: new Date(),
      })
      .run(this.connection)
      .then(({generated_keys}) => this.socket.emit('OPERATION_INITIALIZED', generated_keys[0]))
  }

  join = ({operationId, playerId}) => {
    this.socket.join(operationId, () => {
      r.table(this.table)
        .get(operationId)
        .update({
          players: {
            [playerId]: r.table('users').get(playerId).merge({locked: false})
          }
        }, {non_atomic: true, return_changes: true})
        .run(this.connection)
        .then(({changes}) => {
          changes.forEach(({new_val}) => {
            this.socket.emit('OPERATION_PLAYER_JOINED', new_val); 
          })
        })
    });
  }

  accept = ({operationId, playerId, hostId}) => {
    r.table(this.table)
      .get(operationId)
      .update({
        stage: 'accepted',
        players: {
          [hostId]: {opponentId: playerId},
          [playerId]: {opponentId: hostId}
        }
      })
      .run(this.connection)
      .then(() => {
        this.socket.to(operationId).emit('OPERATION_ACCEPTED', playerId);
      })
  }

  refuse = operationId => {
    r.table(this.table)
      .get(operationId)
      .update({
        stage: 'new',
        players: {
          [operationId]: r.literal()
        },
      })
      .run(this.connection)
  }

  lock = ({userId, operationId}) => {
    r.table(this.table)
      .get(operationId)
      .update({
        players: {
          [userId]: {
            locked: true,
          }
        }
      }, {return_changes: true})
      .run(this.connection)
      .then(({changes}) => {
        changes.forEach(({new_val}) => {
          this.socket.to(operationId).emit('OPERATION_LOCKED_BY', userId);

          if (isOperationLocked(new_val.players)) {
            this.socket.server.in(operationId).emit('OPERATION_LOCKED', randomizeInitialTurn(new_val.players));
          }
        })
      })
  }

  subscribe = operationId => {
    this.socket.join(operationId);

    r.table(this.table)
      .get(operationId)
      .changes({include_initial: true})
      .run(this.connection)
      .then(cursor => {
        cursor.each((err, {new_val}) => {
          this.socket.emit('OPERATION_COMMITED', new_val);
        })
      })
  }
};