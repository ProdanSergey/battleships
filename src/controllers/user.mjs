import r from 'rethinkdb';

export class UserController {
  constructor(socket, connection) {
    this.socket = socket;
    this.connection = connection;
    this.table = 'users';
  }

  connect = user => {
    r.table(this.table)
      .insert({...user, socket: this.socket.id}, {return_changes: true, conflict: 'update'})
      .run(this.connection)
      .then(({changes}) => {
        changes.forEach(({new_val}) => {
          this.socket.emit('USER_CONNECTED', new_val);
        })
      })
  }
};