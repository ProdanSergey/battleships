import express from 'express';
import http from 'http';
import socketIO from 'socket.io';
import rethinkdb from 'rethinkdb';

import {config} from './helpers/db.mjs';
import {setSubscribers} from './subscribers.mjs';

const PORT = process.env.PORT || 5000;

const app = express();
const server = http.createServer(app);
const io = socketIO(server);

const subscribers = setSubscribers(io);

rethinkdb.connect(config).then(subscribers);

server.listen(PORT, () => {
  console.log(`listening on ${PORT} port`);
});
